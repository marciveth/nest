/* eslint-disable prettier/prettier */
import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { UsuarioService } from './usuario.service';

@Controller('usuario')
export class UsuarioController {
constructor (private readonly usuarioService: UsuarioService){}

  @Get('/lista-usuarios')
  getUsuarios() {
    const dataUser = this.usuarioService.listarUsuarios()
    return {
      mensaje: 'obtener lista de usuarios',
      dato:dataUser
    };
  }
  @Get('/getUser/:id')
  getUsuarioId(@Param('id') userId){
    const encontrado = this.usuarioService.buscarPorId(userId)
    if (encontrado.length == 0) {
        return {
            mensaje: 'no hay el usuario con el id solicitado'
        }
    }
    return {
      mensaje: 'user por id ',
      dato: encontrado
    };
  }

  @Get('/:id')
  getporId(@Param('id') idUsuario) {
    const encontrado = this.usuarioService.buscarPorId(idUsuario)
    return {
      mensaje: 'user por id ',
      dato: encontrado
    };
  }
  @Post('crear')
  //sin variable en el body, captura todo el contenido
  crearUsuario(@Body()datosUsuario) {
    console.log(datosUsuario)
    return {
        mensaje: 'usuario creado',
        datos: datosUsuario
    }
  }

  @Put('/actualizar/:id')
  updateUsuario(@Body()datosUsuario, @Param('id') id){
    return {
        mensaje: 'actualizar usuario',
        datos: datosUsuario,
        id:id
    }
  }

  @Delete('/eliminar/:id')
  eliminarUsuario(@Param('id') idUsuario){
    const eliminado = this.usuarioService.eliminarPorId(idUsuario)
    if (!eliminado) {
        return {
            mensaje: 'usuario ya eliminado'
        }
    }
    return {
        mensaje: 'eliminando usuario por id',
        dato:eliminado
    }
  }
}
